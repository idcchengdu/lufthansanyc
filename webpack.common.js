const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CssNextPlugin = require('postcss-cssnext');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const fileStream = require('fs');
const path = require('path');

const publicPath = '../';

module.exports = (env, compileEntries) => {

  const root =  env === 'prod' ? 'prod' : 'dist';

  const config = {
    entry: {},

    output: {
      path: path.resolve(__dirname, root),
      publicPath,
      chunkFilename: 'async-chunks/[name].js',
      filename: `entries/[name].js`,
    },

    devtool: 'inline-source-map',

    module: {
      loaders: [
        {
          test: /\.jsx?$/,
          include: [path.resolve(__dirname, 'src')],

          loader: ['babel-loader'],
        },
        {
          test: /\.scss$/,
          include: [path.resolve(__dirname, 'src')],

          // Note the order of loader applied is opposite with the order within the loaders array
          loader: ExtractTextPlugin.extract([
            {
              loader: 'css-loader',
              options: {
                sourceMap: true,
                modules: false,
                url: true
              },
            },
            {
              loader: 'postcss-loader',
              options: { plugins: () => [CssNextPlugin], sourceMap: true },
            },
            {
              loader: 'resolve-url-loader'
            },

            { loader: 'sass-loader', options: {
              sourceMap: true,
              data: '@import "variables";',
              includePaths: [
                path.join(__dirname, './src/modules/styles')
              ]
            } },
          ]),
        },
        {
          test: /\.css$/,
          include: [path.resolve(__dirname, 'src')],

          // Note the order of loader applied is opposite with the order within the loaders array
          loader: ExtractTextPlugin.extract([
            'css-loader',
            'resolve-url-loader',
          ]),
        },
        {
          test: /\.(png|svg|jpg|gif)$/,
          include: [path.resolve(__dirname, 'src'), path.resolve(__dirname, root), path.resolve(__dirname, 'node_modules')],
          loader: [{
            loader: 'file-loader',
            options: {
              name: '/[name].[ext]',
              outputPath: 'images',
              publicPath,
            },
          }],
        },
        {
          test: /\.pug/,
          include: [path.resolve(__dirname, 'src')],
          loader: [{
            loader: 'pug-loader',
            options: {
              pretty: true,
            },
          }],
        },
        {
          test: /\.hbs/,
          include: [path.resolve(__dirname, 'src')],
          loader: [{
            loader: "handlebars-loader?helperDirs[]="+__dirname+"/src/modules/handlebars-helper"
          }],
        },
        {
          test: /\.(woff|woff2|eot|ttf|otf)(\?[a-z0-9]+)?$/,
          include: [path.resolve(__dirname, 'src')],
          loader: [{
            loader: 'file-loader',
            options: {
              name: '/[name].[ext]',
              outputPath: 'fonts',
              publicPath
            },
          }],
        }],
    },


    resolve: {
      extensions: ['.js', '.jsx']
    },

    plugins: [
      new CleanWebpackPlugin(root),

      // Make css bundle
      new ExtractTextPlugin({ filename: 'style/[name].css', allChunks: true }),

      new webpack.optimize.CommonsChunkPlugin({
        // (the commons chunk name)
        names: ['commons'],

        // (Modules must be shared between 2 entries)
        minChunks: env !== 'local' ? 1 : 2,
      }),

      new CopyWebpackPlugin([
        { from: './src/interface/*', to: 'interface', flatten: true },
      ]),
    ],
  };

  // Get entries from the entries path,
  // if the list of entries need to be compiled is given and the current entry is not included,
  // just don't add it to the config entry collection
  const entries = fileStream.readdirSync('./src/entries').filter(entry =>
    entry !== '.DS_Store' && (env !== 'local' || !compileEntries || !compileEntries.length || compileEntries.includes(entry)));

  entries.forEach((entry) => {
    const localOnlyEntries = [
      // bundle the client for webpack-dev-server
      // and connect to the provided endpoint
      'webpack-dev-server/client?http://10.148.60.75:8080',];


    config.entry[entry] = [
      // Babel polyfill for advanced ES features
      'babel-polyfill',

      // The actual entry
      `./src/entries/${entry}/entry.js`,
    ];

    if (env === 'local') {
      config.entry[entry] = localOnlyEntries.concat(config.entry[entry]);
    }


    config.plugins.push(new HtmlWebpackPlugin({
      chunks: ['commons', entry],
      filename: `./pages/${entry}.html`, // Main html output path
      template: `./src/entries/${entry}/template.pug`, // Html template path
    }));
  });

  config.plugins.push(new HtmlWebpackPlugin({
    inject: false,
    filename: './index.html', // Main html output path
    template: './src/index.pug', // Html template path
    entries,
  }));

  return config;
};

