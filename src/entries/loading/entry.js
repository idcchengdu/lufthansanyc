import '../../modules/helper-modules/base';
import './style.scss';
var article =$("[data-item='article']");

function loadingStarting() {
    var loader = new resLoader({
        resources : window.loadingData.imgs,
        onStart : function(total){
            console.log('start:'+total);
        },
        onProgress : function(current, total){
            console.log(current+'/'+total);

        },
        onComplete : function(total){
            console.log(total);
             location.href = window.loadingData.url;
        }
    });

    $( document ).ready(function() {
        loader.start();
    });

}
function loadingAnimate() {
    article.animate({"width":"85%"},2500,function () {
        article.css({"width":"0%"});
        loadingAnimate();
    })
}
loadingStarting();
loadingAnimate();
