import '../../modules/helper-modules/base';
import './style.scss';
var bg =$("[data-item='bg']");
var animate_redbad =$("[data-animate='redbad']");
var animate_aircraft =$("[data-animate='aircraft']");
var animate_clickbox =$("[data-animate='clickbox']");
// tips
var item_tipsbox2 =$("[data-tips='tipsbox2']");
var item_tipsbox1 =$("[data-tips='tipsbox1']");
var item_tips =$("[data-tips='main-page']");

var n =0;
window.clickFriends= function (box) {
    box?item_tipsbox1.show():item_tipsbox2.show();
    item_tips.show();
}
window.clickTipsIconClose =function (box) {
    box?item_tipsbox1.hide():item_tipsbox2.hide();
    item_tips.hide();
}
function mainAnimate() {
    animate_redbad.animate({"bottom":"25%"},700,function () {
        animate_aircraft.animate({"bottom":"21.5%","right":"25%"},700,function () {
            animate_clickbox.animate({"bottom":"10%"},700,function () {
                  imgsShuffling();
            });
        });
    })
}
function imgsShuffling(){
        n++;
        if(n > bg.length - 1){
            n = 1;
            bg.each(function () {
                $(this).css("display","block");
            })
        }

        bg.each(function () {
            if($(this).data("finish") == n){
                $(this).finish().fadeOut(3000,function () {
                    imgsShuffling();
                });
            }
        });
}
mainAnimate();


