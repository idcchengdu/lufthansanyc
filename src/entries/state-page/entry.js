import '../../modules/helper-modules/base';
import './style.scss';
var state_tips =$("[data-state='tips']");
var state_information_tips =$("[data-state='information-tips']");
var state_invitation =$("[data-state='invitation']");
var state_transmit =$("[data-state='transmit']");
var state_theaward =$("[data-state='theaward']");
var state_theaward_lose =$("[data-state='theaward-lose']");
var state_invitation_img =$("[data-state='invitation-img']");
var state_transmit_img =$("[data-state='transmit-img']");
var state_information_content_tips =$("[data-state='information-content']");
//点击发送邀请
state_invitation.on("click",function () {
    state_tips.show();
    animateStateTips(true);
});
state_theaward.on("click",function () {
    state_information_tips.show();
    state_information_content_tips.css({"height":state_information_content_tips.height(),"bottom":-state_information_content_tips.height()});
    state_information_content_tips.animate({"bottom":"0"},1000);
});
state_theaward_lose.on("click",function () {
    state_information_tips.hide();
});
state_transmit.on("click",function () {
    state_tips.show();
    animateStateTips(false);
});
state_tips.on("click",function () {
    state_tips.hide();
    state_invitation_img.css({"right":"100%","top":"50%"});
    state_transmit_img.css({"right":"100%","top":"50%"});
});
$("[data-from]").on("click",function (e) {
    $(e.target).removeClass("input-error");
});
window.clickInformationFrom=function (url) {
    $("[data-from]").each(function () {
        if($(this).data("from") =="name"){
            var nameVal = $.trim($(this).val());
            if(nameVal!=""){
                $(this).removeClass("input-error");
            }else {
                $(this).addClass("input-error");
            }
        }
        else if($(this).data("from") =="address"){
            var nameVal = $.trim($(this).val());
            if(nameVal!=""){
                $(this).removeClass("input-error");
            }else {
                $(this).addClass("input-error");
            }

        }else if($(this).data("from") =="phone"){
            var nameVal = $.trim($(this).val());
            var regName = /^1[34578]\d{9}$/;
            if(nameVal!="" && regName.test(nameVal)){
                $(this).removeClass("input-error");
            }else {
                $(this).addClass("input-error");
            }
        }
    });
    if($("from").find(".input-error").length == 0){
        var state_arry={};
        state_arry.name = $("from").find("[data-from='name']").val();
        state_arry.address = $("from").find("[data-from='address']").val();
        state_arry.phone = $("from").find("[data-from='phone']").val();
        state_arry.time =new Date().getTime();
        if(url){
            window.location.href = url +$.param(state_arry);
        }
    }
}
//图片从左往右上固定飞出
function animateStateTips(ifImg) {
    ifImg?state_invitation_img.animate({right: "10%",top: "3%"},1000):state_transmit_img.animate({right: "10%",top: "3%"},1000);
}
