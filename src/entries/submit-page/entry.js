import '../../modules/helper-modules/base';
import './style.scss';
var animate_submit_page =$("[data-animate='submit-page']");
var animate_submit_bg =$("[data-animate='submit-bg']");
var animate_cover =$("[data-animate='cover']");
var from_select =$("[data-from='select']");
var from_bg =$("[data-from='bg']");
var submit_tips_bg =$("[data-submit='tips']");
var submit_lose_icon = $("[data-submit='lose-icon']");
window.clickSubmitPage= function (url) {
    from_bg.each(function () {
        if($(this).data("val") =="" || $(this).data("val") ==undefined || $(this).data("val")==null) {
            $(this).addClass("verification-error");
        }
    });
    if($("from").find(".verification-error").length == 0) {
        var submit_arry={};
        submit_arry.city1 =$("from").find("[data-selectname='city1']").data("val");
        submit_arry.people =$("from").find("[data-selectname='people']").data("val");
        submit_arry.city2 =$("from").find("[data-selectname='city2']").data("val");
        submit_arry.time =new Date().getTime();
        if(url){
            window.location.href = url + $.param(submit_arry);
        }
    }
}
window.showSubmitPageTips=function () {
    submit_tips_bg.show();
}
submit_lose_icon.on("click",function () {
    submit_tips_bg.hide();
})
from_select.on("change",function (e) {
   var select=  $(e.target);
   var parent = select.parent();
   if(select.val()){
       parent.find("[data-from='bg']").text(select.val());
       parent.find("[data-from='bg']").data("val",select.val());
       parent.find("[data-from='bg']").removeClass("verification-error");
   }else {
       parent.find("[data-from='bg']").text( parent.find("[data-from='bg']").data("selected"));
       parent.find("[data-from='bg']").data("val","");
       parent.find("[data-from='bg']").addClass("verification-error");
   }
});
function submitPageAnimate() {
    animate_submit_page.animate({"bottom":"0"},500,function () {
           animate_cover.addClass("redbag-cover-show");
           animate_submit_bg.animate({"bottom":"12%"},1500);
    });
}
submitPageAnimate();