module.exports = function (price) {
  if (/[^0-9\.]/.test(price)){
    return price
  }
  price = price.toString();
  price = price.replace(/^(\d*)$/, "$1.");
  price = (price + "00").replace(/(\d*\.\d\d)\d*/, "$1");
  price = price.replace(".", ",");
  var re = /(\d)(\d{3},)/;
  while (re.test(price))
    price = price.replace(re, "$1,$2");
  price = price.replace(/,(\d\d)$/, ".$1");
  return price.replace(/^\./, "0.")
}
