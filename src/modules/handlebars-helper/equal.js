module.exports = function (a, b, options) {
  return options[a == b ? 'fn' : 'inverse'](this)
}
