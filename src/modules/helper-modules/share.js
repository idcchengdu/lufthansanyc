// 通用分享文字：
// 标题：超好玩的圣诞游戏，正在送网红小家电。描述文字：每天都来玩，中奖机会多。
// 描述文字：每天都来玩,中奖机会多

// 游戏分享文字：
// 标题：人类奇迹在此，我居然能快到XXX秒。
// 描述文字：你们凡人围观就好。
//
import jQuery from 'jquery';
window.$ = jQuery.noConflict();
var ts=Math.round(+new Date()/1000);
var signature = sha1();
var urls=location.href.split('#')[0];
var n =0;
var shareUrl="";
var mainshareUrl ="http://www.mlpsh.cn/lufthansa/2018cny/main.php";
var share_title='新年游欧洲，往返机票免费送!';
var share_desc='我正在参加“汉莎新年，团聚欧洲”活动，赢取免费往返欧洲机票，你也来参与吧！';
var share_peopele_desc="快来加入我的欧洲旅行亲友团，免费赢取汉莎航空欧洲往返机票";
var share_image='http://www.mlpsh.cn/lufthansa/2018cny/dist/images/wx_bg.jpg';
var state_page_url=$("[data-statepage='only-page']");
if(state_page_url.length && state_page_url.length > 0 && state_page_url.data("statepageurl")){
    shareUrl=state_page_url.data("statepageurl");
}else {
    shareUrl="http://www.mlpsh.cn/lufthansa/2018cny/main.php";
}

$.ajax({
    url: "http://www.mlpsh.cn/lufthansa/2018cny/read_token.php",
    dataType: "json",
    success: function( response ) {
        // alert('进来了');
        // debugger;
        // alert('access_token:'+response);
        call_wx(response.access_token,ts);

    },
    error: function (jqXHR, exception) {
        var msg = '';
        if (jqXHR.status === 0) {
            msg = 'Not connect.\n Verify Network.';
        } else if (jqXHR.status == 404) {
            msg = 'Requested page not found. [404]';
        } else if (jqXHR.status == 500) {
            msg = 'Internal Server Error [500].';
        } else if (exception === 'parsererror') {
            msg = 'Requested JSON parse failed.';
        } else if (exception === 'timeout') {
            msg = 'Time out error.';
        } else if (exception === 'abort') {
            msg = 'Ajax request aborted.';
        } else {
            msg = 'Uncaught Error.\n' + jqXHR.responseText;
        }
        // debugger;
        // alert('没进来'+msg);
    }
});
    
function call_wx(access_token,ts){
    $.ajax({
    url: "http://www.mlpsh.cn/lufthansa/2018cny/read_ticket.php?token="+access_token,
	    dataType: "json",
	    success: function( response ) {
          var snKey="jsapi_ticket="+response.ticket+"&noncestr=Wm3WZYTPz0wzccn&timestamp="+ts+"&url="+urls;
	        var signature=sha1(snKey);
          
	        wx.config({
		        debug: false,
		        appId: "wx75561533f3e210bb",
		        timestamp: ts,
		        nonceStr: "Wm3WZYTPz0wzccn",
		        signature: signature,
		        jsApiList: [
		            'checkJsApi',
		            'onMenuShareTimeline',
		            'onMenuShareAppMessage',
                'scanQRCode'
		        ]
		    });

		    wx.ready(function () {
		        
            wx.checkJsApi({
		            jsApiList: [
		              'onMenuShareTimeline',
		            	'onMenuShareAppMessage'
		            ]
		        });

            wx.showOptionMenu();

		        wx.onMenuShareAppMessage({
				    title: share_title, // 分享标题
				    desc: share_peopele_desc, // 分享描述
                    link: shareUrl,
				    imgUrl: share_image, // 分享图标
				    type: '', // 分享类型,music、video或link，不填默认为link
				    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
                    trigger: function (res) {
                    },
				    success: function () {
                      // debugger;
                        if($("[data-state='tips']").length > 0 &&$("[data-state='tips']").length){
                            $("[data-state='tips']").hide();
                            $("[data-state='invitation-img']").css({"right":"100%","top":"50%"});
                            $("[data-state='transmit-img']").css({"right":"100%","top":"50%"});
                        }
                        if (typeof(gtag) == 'function'){
                            gtag('event', 'Button', { 'event_category': 'Post',
                                'event_label': 'friends' });
                        }
				    },
				    cancel: function () {
                        // debugger;
                        if($("[data-state='tips']").length > 0 &&$("[data-state='tips']").length){
                            $("[data-state='tips']").hide();
                            $("[data-state='invitation-img']").css({"right":"100%","top":"50%"});
                            $("[data-state='transmit-img']").css({"right":"100%","top":"50%"});
                        }
                    // $('.mask').hide();
                    // ga('send', 'event', 'wShare', 'AppMessage', 'cancel');
				    },
                    fail: function (res) {
                        // debugger;
                    }
				});
		        wx.onMenuShareTimeline({
		            title: share_desc,
                    link: mainshareUrl,
		            imgUrl: share_image,
		            trigger: function (res) {
		                // alert('用户点击分享到朋友圈' +JSON.stringify(res));
		                // debugger;
		            },
		            success: function (res) {
		                // alert('已分享');
                        if($("[data-state='tips']").length > 0 &&$("[data-state='tips']").length){
                            $("[data-state='tips']").hide();
                            $("[data-state='invitation-img']").css({"right":"100%","top":"50%"});
                            $("[data-state='transmit-img']").css({"right":"100%","top":"50%"});
                        }
                        if (typeof(gtag) == 'function'){
                            gtag('event', 'Button', { 'event_category': 'Post',
                                'event_label': 'moments' });
                        }
		            },
		            cancel: function (res) {
		                // alert('已取消');
                        if($("[data-state='tips']").length > 0 &&$("[data-state='tips']").length){
                            $("[data-state='tips']").hide();
                            $("[data-state='invitation-img']").css({"right":"100%","top":"50%"});
                            $("[data-state='transmit-img']").css({"right":"100%","top":"50%"});
                        }
                    // $('.mask').hide();
                    // ga('send', 'event', 'wShare', 'Timeline', 'cancel');
		            },
		            fail: function (res) {
		                // alert('wx.onMenuShareTimeline:fail: '+JSON.stringify(res));
                    // $('.mask').hide();
		            }
		        });
		    });
		    wx.error(function (res) {
                n ++;
                if(n < 3){
                    $.ajax({
                        url: "http://www.mlpsh.cn/lufthansa/2018cny/read_token.php?refresh=force",
                        dataType: "json",
                        success: function( response ) {
                            call_wx(response.access_token,ts);

                        },
                        error: function (jqXHR, exception) {
                        }
                    });
                }
		    });
			        // 調用api  end
			    }
			}

      );
}



function sha1(str) {
  var rotate_left = function(n, s) {
    var t4 = (n << s) | (n >>> (32 - s));
    return t4;
  };
  var cvt_hex = function(val) {
    var str = '';
    var i;
    var v;

    for (i = 7; i >= 0; i--) {
      v = (val >>> (i * 4)) & 0x0f;
      str += v.toString(16);
    }
    return str;
  };

  var blockstart;
  var i, j;
  var W = new Array(80);
  var H0 = 0x67452301;
  var H1 = 0xEFCDAB89;
  var H2 = 0x98BADCFE;
  var H3 = 0x10325476;
  var H4 = 0xC3D2E1F0;
  var A, B, C, D, E;
  var temp;

  str = utf8_encode(str);
  var str_len = str.length;

  var word_array = [];
  for (i = 0; i < str_len - 3; i += 4) {
    j = str.charCodeAt(i) << 24 | str.charCodeAt(i + 1) << 16 | str.charCodeAt(i + 2) << 8 | str.charCodeAt(i + 3);
    word_array.push(j);
  }

  switch (str_len % 4) {
    case 0:
      i = 0x080000000;
      break;
    case 1:
      i = str.charCodeAt(str_len - 1) << 24 | 0x0800000;
      break;
    case 2:
      i = str.charCodeAt(str_len - 2) << 24 | str.charCodeAt(str_len - 1) << 16 | 0x08000;
      break;
    case 3:
      i = str.charCodeAt(str_len - 3) << 24 | str.charCodeAt(str_len - 2) << 16 | str.charCodeAt(str_len - 1) <<
        8 | 0x80;
      break;
  }

  word_array.push(i);

  while ((word_array.length % 16) != 14) {
    word_array.push(0);
  }

  word_array.push(str_len >>> 29);
  word_array.push((str_len << 3) & 0x0ffffffff);

  for (blockstart = 0; blockstart < word_array.length; blockstart += 16) {
    for (i = 0; i < 16; i++) {
      W[i] = word_array[blockstart + i];
    }
    for (i = 16; i <= 79; i++) {
      W[i] = rotate_left(W[i - 3] ^ W[i - 8] ^ W[i - 14] ^ W[i - 16], 1);
    }

    A = H0;
    B = H1;
    C = H2;
    D = H3;
    E = H4;

    for (i = 0; i <= 19; i++) {
      temp = (rotate_left(A, 5) + ((B & C) | (~B & D)) + E + W[i] + 0x5A827999) & 0x0ffffffff;
      E = D;
      D = C;
      C = rotate_left(B, 30);
      B = A;
      A = temp;
    }

    for (i = 20; i <= 39; i++) {
      temp = (rotate_left(A, 5) + (B ^ C ^ D) + E + W[i] + 0x6ED9EBA1) & 0x0ffffffff;
      E = D;
      D = C;
      C = rotate_left(B, 30);
      B = A;
      A = temp;
    }

    for (i = 40; i <= 59; i++) {
      temp = (rotate_left(A, 5) + ((B & C) | (B & D) | (C & D)) + E + W[i] + 0x8F1BBCDC) & 0x0ffffffff;
      E = D;
      D = C;
      C = rotate_left(B, 30);
      B = A;
      A = temp;
    }

    for (i = 60; i <= 79; i++) {
      temp = (rotate_left(A, 5) + (B ^ C ^ D) + E + W[i] + 0xCA62C1D6) & 0x0ffffffff;
      E = D;
      D = C;
      C = rotate_left(B, 30);
      B = A;
      A = temp;
    }

    H0 = (H0 + A) & 0x0ffffffff;
    H1 = (H1 + B) & 0x0ffffffff;
    H2 = (H2 + C) & 0x0ffffffff;
    H3 = (H3 + D) & 0x0ffffffff;
    H4 = (H4 + E) & 0x0ffffffff;
  }

  temp = cvt_hex(H0) + cvt_hex(H1) + cvt_hex(H2) + cvt_hex(H3) + cvt_hex(H4);
  return temp.toLowerCase();
}    
function utf8_encode( string ) {
return unescape( encodeURIComponent( string ) );
}
function utf8_decode( string ) {
return decodeURIComponent( escape( string ) );
}